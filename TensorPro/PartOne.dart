// https://youtu.be/8F2uemqLwvE?list=PLJbE2Yu2zumDjfrfu8kisK9lQVcpMDDzZ
// Variables (3:58 sec start)
// Types (5:32 sec start)
// Functions (10:03 sec start)
// Objects

void main() {
  var a = -10;
  print(a.abs()); //abs() Returns the absolute value
  
  
  /* Built In Types (these are the 7 main primitive data types)
   * numbers - int, float
   * strings - "Hello!"
   * boolean - true, false
   * lists - (aka array), List<int>
   * maps - hashmaps
   * runes - unicode character sets
   * symbols - #symbols
   */
  int x = 10;
  double y = 10.0;
  String s = "${x + y}";
  print(s);
  bool b = true;
  print(b);
  List l = [1, 2, 3];
  print(l[0]);
  List<String> ls = ['1', '2', '3']; // only string type data
  print(ls[1]);
  
  Map<String, int> map = {
    "Class": 6,
    "Age": 11,
    "Girls": 9,
  };
  print(map);
  
  // Function Example
  //print(add(1, 2));
  //print(add(20.0, 30.0));
  //print(add("a", "b"));
  
  var result = operators[0](10, 20);
  print("Result is $result");
  
}
//int add(int a, int b) {
//  return a + b;
//}
int add(int a, int b) => a + b;
int sub(int a, int b) => a - b;

List operators = [add, sub];

//exec(Function op, x, y) {
//  return op(x, y);
//}
//choose(bool op) {
//  if (op == true) {
//    return add;
//  } else {
//    return sub;
//  }
//}

