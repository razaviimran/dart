// Pascal Triangle

import 'dart:io';

void main() {

	for (int i = 1; i <= 5; i++) {

		for (int j = 5; j > i; j--) {
			stdout.write(' ');
		}

		int wildCard = 1;
		for (int k = 1; k <= i; k++) {
			stdout.write('$wildCard ');
			wildCard = ${wildCard} * (${i} - ${k}) / ${k};
		}
		print('');

	}
	
}

